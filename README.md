# WebWatcher #

Simple website life-checker. It has been created purely situational, my girlfriend wanted to visit a particular website, but due to high traffic the website was down. In order to monitor website health I have created this pinging application. It was reviewed, few functionalieties and small tidy up was performed. Tests have been rewritten.

I used this opportunity to improve my WPF MVVM skills - hence Caliburn.micro is utilized.

### How do I get set up? ###

* Prerequisites: .NET Framework 4.8 runtime, Visual Studio,
* Download ZIP or clone repository,
* Open TaskManager.sln
* Run and enjoy!

### Additional packages used in the project ###

* [Caliburn.micro](https://caliburnmicro.com/)
* [Autofac](https://autofac.org/)
* [NUnit](https://nunit.org/)
* [Moq](https://github.com/moq/moq4)
* [Wpf Toolkit](https://github.com/xceedsoftware/wpftoolkit)