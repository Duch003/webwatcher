﻿namespace WebWatcher.UI.Models
{
    public enum Command
    {
        Start,
        Stop,
        Reset,
        InvalidUrl,
        ValidUrl,
        UrlNotFound,
        UrlFound
    }
}
