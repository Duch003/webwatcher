﻿using System;
using System.Net;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Models
{
    public class HttpWebResponseAdapter : IHttpWebResponseAdapter
    {
        private HttpWebResponse _response;

        public HttpWebResponseAdapter(HttpWebResponse response)
        {
            _response = response;
        }

        public HttpStatusCode StatusCode { get => _response.StatusCode; }
        public Uri ResponseUri { get => _response.ResponseUri; }
    }
}
