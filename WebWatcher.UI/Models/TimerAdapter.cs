﻿using System.Timers;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Models
{
    public class TimerAdapter : ITimerAdapter
    {
        private Timer _timer = new Timer();

        public bool AutoReset { get => _timer.AutoReset; set => _timer.AutoReset = value; }
        public bool Enabled { get => _timer.Enabled; set => _timer.Enabled = value; }
        public double Interval { get => _timer.Interval; set => _timer.Interval = value; }

        event ElapsedEventHandler ITimerAdapter.Elapsed
        {
            add
            {
                _timer.Elapsed += value;
            }

            remove
            {
                _timer.Elapsed -= value;
            }
        }

        public void Start() => _timer.Start();
        public void Stop() => _timer.Stop();
    }
}
