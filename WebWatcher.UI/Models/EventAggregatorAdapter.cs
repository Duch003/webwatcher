﻿using Caliburn.Micro;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Models
{
    public class EventAggregatorAdapter : IEventAggregatorAdapter
    {
        private IEventAggregator _eventAggregator;

        public EventAggregatorAdapter(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        public void SubscribeOnPublishedThread(object subscriber)
        {
            _eventAggregator.SubscribeOnPublishedThread(subscriber);
        }

        public async Task PublishOnUIThreadAsync(object message)
        {
            await _eventAggregator.PublishOnUIThreadAsync(message);
        }
    }
}
