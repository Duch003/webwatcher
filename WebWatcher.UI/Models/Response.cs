﻿using System;
using System.Net;

namespace WebWatcher.UI.Models
{
    public class Response
    {
        public State State { get; set; }
        public HttpStatusCode Status { get; set; }
        public DateTime DateTime { get; set; }
        public string Url { get; set; }
    }
}
