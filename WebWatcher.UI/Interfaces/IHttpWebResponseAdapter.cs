﻿using System;
using System.Net;

namespace WebWatcher.UI.Interfaces
{
    public interface IHttpWebResponseAdapter
    {
        HttpStatusCode StatusCode { get; }
        Uri ResponseUri { get; }
    }
}
