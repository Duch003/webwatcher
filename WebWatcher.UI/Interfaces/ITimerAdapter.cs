﻿using System.Timers;

namespace WebWatcher.UI.Interfaces
{
    public interface ITimerAdapter
    {
        public event ElapsedEventHandler Elapsed;
        public bool AutoReset { get; set; }
        public bool Enabled { get; set; }
        public double Interval { get; set; }
        public void Start();
        public void Stop();
    }
}
