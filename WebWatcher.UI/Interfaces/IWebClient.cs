﻿using System.Threading.Tasks;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Interfaces
{
    public interface IWebClient
    {
        Task<Response> GetAsync(string url);
    }
}