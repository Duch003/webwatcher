﻿using System.Threading.Tasks;

namespace WebWatcher.UI.Interfaces
{
    public interface IEventAggregatorAdapter
    {
        Task PublishOnUIThreadAsync(object message);
        void SubscribeOnPublishedThread(object subscriber);
    }
}