﻿using System.Threading.Tasks;

namespace WebWatcher.UI.Interfaces
{
    public interface IWebRequestor
    {
        Task<IHttpWebResponseAdapter> GetAsync(string url);
    }
}