﻿namespace WebWatcher.UI.Interfaces
{
    public interface ITimerFactory
    {
        ITimerAdapter GetTimer();
    }
}