﻿using Caliburn.Micro;
using System;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Interfaces
{
    public interface IUCLogPanelViewModel : IScreen, IHandle<Message<IUCLogPanelViewModel, Response>>,
        IHandle<Message<IUCLogPanelViewModel, Command>>, IHandle<Message<IUCLogPanelViewModel, Exception>>
    {
        string Notes { get; set; }
    }
}