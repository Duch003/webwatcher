﻿using Caliburn.Micro;
using System;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Interfaces
{
    public interface IUCMessageBoxViewModel : IScreen, IHandle<Message<IUCMessageBoxViewModel, Exception>>,
        IHandle<Message<IUCMessageBoxViewModel, Response>>, IHandle<Message<IUCMessageBoxViewModel, Command>>
    {
        string State { get; set; }
        string Message { get; set; }
    }
}