﻿using System;

namespace WebWatcher.UI.Interfaces
{
    public interface IDateTimeFactory
    {
        DateTime GetNow();
    }
}