﻿using System.Threading.Tasks;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Interfaces
{
    public interface IWebService
    {
        Task<Result<Response>> CheckPageAsync(string url);
    }
}