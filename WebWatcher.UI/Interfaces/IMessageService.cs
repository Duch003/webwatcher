﻿using System;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Interfaces
{
    public interface IMessageService
    {
        Result<string> ResponseToText(Response log);
        Result<string> CommandToText(Command command);
        Result<string> ExceptionToText(Exception e);
    }
}