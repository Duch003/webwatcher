﻿using System.Timers;

namespace WebWatcher.UI.Interfaces
{
    public interface ITimerService
    {
        void Reset();
        void Start(double interval);
        void Stop();
        void AddEventHandler(ElapsedEventHandler elapsedEventHandler);
        void RemoveEventHandler(ElapsedEventHandler elapsedEventHandler);
        bool Enabled { get; }
    }
}