﻿using System;
using System.Text.RegularExpressions;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Validators
{
    public class UrlValidator : IUrlValidator
    {
        private string _urlPattern = @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";

        public bool IsUrlValid(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return false;
            }

            if(!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                return false;
            }

            if(!Regex.IsMatch(url, _urlPattern))
            {
                return false;
            }

            if(url == "https://")
            {
                return false;
            }

            if(url == "http://")
            {
                return false;
            }

            return true;
        }
    }
}
