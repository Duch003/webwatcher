﻿using System;
using System.Net;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Mappers
{
    public static class HttpWebResponseMapper
    {
        public static Response MapToResponse(this IHttpWebResponseAdapter response, IDateTimeFactory dateTimeFactory)
        {
            if (dateTimeFactory is null)
            {
                throw new ArgumentNullException(nameof(dateTimeFactory));
            }

            if (IsSuccessfull(response.StatusCode))
            {
                return new Response
                {
                    DateTime = dateTimeFactory.GetNow(),
                    Status = response.StatusCode,
                    Url = response.ResponseUri.ToString(),
                    State = State.Ok
                };
                
            }

            if (IsFailure(response.StatusCode))
            {
                return new Response
                {
                    DateTime = dateTimeFactory.GetNow(),
                    Status = response.StatusCode,
                    Url = response.ResponseUri.ToString(),
                    State = State.Error
                };
            }
          
            return new Response
            {
                DateTime = dateTimeFactory.GetNow(),
                Status = response.StatusCode,
                Url = response.ResponseUri.ToString(),
                State = State.Warning
            };
        }

        private static bool IsSuccessfull(HttpStatusCode statusCode)
        {
            return (int)statusCode >= 200 && (int)statusCode < 300 && Enum.IsDefined(typeof(HttpStatusCode), (int)statusCode);
        }

        private static bool IsFailure(HttpStatusCode statusCode)
        {
            return (int)statusCode >= 400 && (int)statusCode < 600 && Enum.IsDefined(typeof(HttpStatusCode), (int)statusCode);
        }
    }
}
