﻿using System;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Factories
{
    public class DateTimeFactory : IDateTimeFactory
    {
        public DateTime GetNow() => DateTime.Now;
    }
}
