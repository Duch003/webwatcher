﻿using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Factories
{
    public class TimerFactory : ITimerFactory
    {
        public ITimerAdapter GetTimer() => new TimerAdapter();
    }
}
