﻿using System.Net;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Mappers;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Clients
{
    public class CustomWebClient : IWebClient
    {
        private readonly IWebRequestor _webRequestor;
        private readonly IDateTimeFactory _dateTimeFactory;

        public CustomWebClient(IWebRequestor webRequestor, IDateTimeFactory dateTimeFactory)
        {
            _webRequestor = webRequestor;
            _dateTimeFactory = dateTimeFactory;
        }

        public async Task<Response> GetAsync(string url)
        {
            try
            {
                return (await _webRequestor.GetAsync(url)).MapToResponse(_dateTimeFactory);
            }
            catch (WebException we)
            {
                if(we.Response == null)
                {
                    throw;
                }
                return new HttpWebResponseAdapter((HttpWebResponse)we.Response).MapToResponse(_dateTimeFactory);
            }
            catch
            {
                throw;
            }
        }
    }
}
