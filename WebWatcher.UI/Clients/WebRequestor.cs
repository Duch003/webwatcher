﻿using System.Net;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Clients
{
    public class WebRequestor : IWebRequestor
    {
        public async Task<IHttpWebResponseAdapter> GetAsync(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Timeout = 5000;
            var response = (HttpWebResponse) await request.GetResponseAsync();
            return new HttpWebResponseAdapter(response);
        }
    }
}
