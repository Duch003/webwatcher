﻿using Caliburn.Micro;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.ViewModels
{
    public class UCMessageBoxViewModel : Screen, IUCMessageBoxViewModel
    {
        private string _state;
        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                NotifyOfPropertyChange();
            }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set 
            { 
                _message = value;
                NotifyOfPropertyChange();
            }
        }

        private readonly IEventAggregatorAdapter _eventAggregator;

        public UCMessageBoxViewModel(IEventAggregatorAdapter eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.SubscribeOnPublishedThread(this);
            SetInitialState();
        }

        #region Handlers
        public async Task HandleAsync(Message<IUCMessageBoxViewModel, Response> message, CancellationToken cancellationToken)
        {
            await Task.Run(() => HandleMessage(message));
        }

        public async Task HandleAsync(Message<IUCMessageBoxViewModel, Exception> message, CancellationToken cancellationToken)
        {
            await Task.Run(() => HandleMessage(message));
        }

        public async Task HandleAsync(Message<IUCMessageBoxViewModel, Command> message, CancellationToken cancellationToken)
        {
            await Task.Run(() => HandleMessage(message));
        }
        #endregion

        #region Private methods
        private void SetInitialState()
        {
            Message = "Ready to go!";
            State = Models.State.Ok.ToString();
        }

        private void HandleMessage(Message<IUCMessageBoxViewModel, Command> message)
        {
            switch (message.Value)
            {
                case Command.Start:
                    State = Models.State.Warning.ToString();
                    Message = "Running...";
                    break;
                case Command.Stop:
                    State = Models.State.Warning.ToString();
                    Message = "Ready to resume.";
                    break;
                case Command.ValidUrl:
                case Command.Reset:
                    State = Models.State.Ok.ToString();
                    Message = "Ready to go!";
                    break;
                case Command.InvalidUrl:
                    State = Models.State.Error.ToString();
                    Message = "Url is invalid.";
                    break;
                case Command.UrlNotFound:
                    State = Models.State.Error.ToString();
                    Message = "It seems like given url does not exist.";
                    break;
                default:
                    State = Models.State.Error.ToString();
                    Message = $"Unknown command received: {message.Value}";
                    break;
            }
        }

        private void HandleMessage(Message<IUCMessageBoxViewModel, Response> message)
        {
            if(message.Value == null)
            {
                State = Models.State.Error.ToString();
                Message = "Error: Response was null.";
                return;
            }
            State = message.Value.State.ToString();
            Message = HttpWorkerRequest.GetStatusDescription((int)message.Value.Status);
        }

        private void HandleMessage(Message<IUCMessageBoxViewModel, Exception> message)
        {
            State = Models.State.Error.ToString();

            if (message.Value is null)
            {
                Message = "Unknown error.";
                return;
            }

            Message = MessageBuilder(message.Value);
        }

        private string MessageBuilder(Exception exception, int tab = 0)
        {
            var builder = new StringBuilder();

            builder.AppendLine($"MESSAGE:");
            builder.AppendLine($"{new string('\t', tab)}{exception.Message}");

            if (exception.InnerException != null)
            {
                builder.Append($"{new string('\t', ++tab)}INNER ");
                builder.AppendLine($"{new string('\t', tab)}{MessageBuilder(exception.InnerException, tab)}");
            }

            return builder.ToString();
        }
        #endregion
    }
}
