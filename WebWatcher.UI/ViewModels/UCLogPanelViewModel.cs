﻿using Caliburn.Micro;
using System;
using System.Threading;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.ViewModels
{
    public class UCLogPanelViewModel : Screen, IUCLogPanelViewModel
    {
        private string _notes;
        public string Notes
        {
            get 
            {
                return _notes;
            }
            set
            {
                _notes = value;
                NotifyOfPropertyChange();
            }
        }

        private readonly IEventAggregatorAdapter _eventAggregator;
        private readonly IMessageService _logService;

        public UCLogPanelViewModel(IEventAggregatorAdapter eventAggregator, IMessageService logService)
        {
            _eventAggregator = eventAggregator;
            _logService = logService;
            _eventAggregator.SubscribeOnPublishedThread(this);
            Notes = string.Empty;
        }

        #region Handlers
        public async Task HandleAsync(Message<IUCLogPanelViewModel, Response> message, CancellationToken cancellationToken)
        {
            await HandleMessageAsync(message);
        }

        public async Task HandleAsync(Message<IUCLogPanelViewModel, Command> message, CancellationToken cancellationToken)
        {
            await HandleMessageAsync(message);
        }

        public async Task HandleAsync(Message<IUCLogPanelViewModel, Exception> message, CancellationToken cancellationToken)
        {
            await HandleMessageAsync(message);
        }
        #endregion

        #region Private methods
        private async Task HandleMessageAsync(Message<IUCLogPanelViewModel, Command> message)
        {
            var result = _logService.CommandToText(message.Value);
            if (!result.IsFine)
            {
                await _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Exception>(result.Exception));
                return;
            }
            Notes += result.Output;
        }

        private async Task HandleMessageAsync(Message<IUCLogPanelViewModel, Response> message)
        {
            var result = _logService.ResponseToText(message.Value);
            if (!result.IsFine)
            {
                await _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Exception>(result.Exception));
                return;
            }

            Notes += result.Output;
        }

        private async Task HandleMessageAsync(Message<IUCLogPanelViewModel, Exception> message)
        {
            var result = _logService.ExceptionToText(message.Value);
            if (!result.IsFine)
            {
                await _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Exception>(result.Exception));
                return;
            }

            Notes += result.Output;
        }
        #endregion
    }
}
