﻿using Caliburn.Micro;
using System.Timers;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.ViewModels
{
    public class RootViewModel : Conductor<IScreen>.Collection.AllActive
    {
        protected IWebService _webService;
        protected IEventAggregatorAdapter _eventAggregator;
        protected Timer _timer;

        private int _timeInMinutes;
        public int TimeInMinutes
        {
            get { return _timeInMinutes; }
            set 
            {
                _timeInMinutes = value;
                NotifyOfPropertyChange();
            }
        }

        private IUCLogPanelViewModel _uCLogPanelViewModel;
        public IUCLogPanelViewModel UCLogPanelViewModel
        {
            get { return _uCLogPanelViewModel; }
            set 
            {
                _uCLogPanelViewModel = value;
                NotifyOfPropertyChange();
            }
        }

        private IUCMessageBoxViewModel _uCMessageBoxViewModel;
        public IUCMessageBoxViewModel UCMessageBoxViewModel
        {
            get { return _uCMessageBoxViewModel; }
            set 
            {
                _uCMessageBoxViewModel = value;
                NotifyOfPropertyChange();
            }
        }

        private IUCControlPanelViewModel _uCControlPanelViewModel;
        public IUCControlPanelViewModel UCControlPanelViewModel
        {
            get { return _uCControlPanelViewModel; }
            set 
            { 
                _uCControlPanelViewModel = value;
                NotifyOfPropertyChange();
            }
        }

        public RootViewModel(IWebService webService, IEventAggregatorAdapter eventAggregator)
        {
            _webService = webService;
            _eventAggregator = eventAggregator;
            _eventAggregator.SubscribeOnPublishedThread(this);

            UCMessageBoxViewModel = Startup.Resolve<IUCMessageBoxViewModel>();
            UCLogPanelViewModel = Startup.Resolve<IUCLogPanelViewModel>();
            UCControlPanelViewModel = Startup.Resolve<IUCControlPanelViewModel>();
        }
    }
}
