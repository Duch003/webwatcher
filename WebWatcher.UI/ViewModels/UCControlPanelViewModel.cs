﻿using Caliburn.Micro;
using System;
using System.Net;
using System.Timers;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.ViewModels
{
    public class UCControlPanelViewModel : Screen, IUCControlPanelViewModel
    {
        private bool _canChangeAddress;
        public bool CanChangeAddress
        {
            get { return _canChangeAddress; }
            private set 
            { 
                _canChangeAddress = value;
                NotifyOfPropertyChange();
            }
        }

        private string _state;
        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                NotifyOfPropertyChange();
            }
        }

        private double _time;
        public double Time
        {
            get { return _time; }
            set
            {
                var val = Math.Floor(value);
                if(val < 10 || double.IsNaN(val))
                {
                    _time = 10;
                }
                else if(val > 120)
                {
                    _time = 120;
                }
                else
                {
                    _time = val;
                }
                NotifyOfPropertyChange();
            }
        }
        public double MultipliedTime => Time * 1000;

        private string _url;
        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                _canStart = ValidateUrl();
                NotifyOfPropertyChange();
                NotifyOfPropertyChange(() => CanStart);
            }
        }

        private IEventAggregatorAdapter _eventAggregator;
        private IWebService _webService;
        private IUrlValidator _urlValidator;
        private ITimerService _timerService;
        private IDateTimeFactory _dateTimeFactory;
        private bool _canStart = false;

        public UCControlPanelViewModel(IEventAggregatorAdapter eventAggregator, IWebService webService, IUrlValidator urlValidator, 
            ITimerService timerService, IDateTimeFactory dateTimeFactory)
        {
            _eventAggregator = eventAggregator;
            _webService = webService;
            _urlValidator = urlValidator;
            _timerService = timerService;
            _dateTimeFactory = dateTimeFactory;
            _timerService.AddEventHandler(OnElapsedEventHandler);
            _eventAggregator.SubscribeOnPublishedThread(this);
            CanChangeAddress = true;
            Time = 10;
        }

        #region Public methods

        public bool CanStart => !_timerService.Enabled && _canStart;
        public async void Start()
        {
            if(!CanStart)
            {
                return;
            }

            var result = await _webService.CheckPageAsync(Url);
            if (!result.IsFine)
            {
                Reset();
                Publish(result.Exception);
                return;
            }

            if (result.Output.State != Models.State.Ok)
            {
                Reset();
                Publish(result.Output);
                return;
            }

            CanChangeAddress = false;
            _timerService.Start(MultipliedTime);
            Publish(Command.Start);
            NotifyAll();
        }

        public bool CanStop => _timerService.Enabled;
        public void Stop()
        {
            if (!CanStop)
            {
                return;
            }

            _timerService.Stop();
            CanChangeAddress = true;
            Publish(Command.Stop);
            NotifyAll();
        }

        public bool CanReset => !_timerService.Enabled;
        public void Reset()
        {
            if (!CanReset)
            {
                return;
            }

            _timerService.Reset();
            CanChangeAddress = true;
            Publish(Command.Reset);
            NotifyAll();
        }

        #endregion

        #region Private methods
        private async void OnElapsedEventHandler(object sender, ElapsedEventArgs e)
        {
            var result = await _webService.CheckPageAsync(Url);
            if (!result.IsFine)
            {
                await _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Exception>(result.Exception));
                Reset();
                return;
            }

            await _eventAggregator.PublishOnUIThreadAsync(new Message<IUCLogPanelViewModel, Response>(result.Output));
        }

        private bool ValidateUrl()
        {
            var result = _urlValidator.IsUrlValid(Url);

            if (!result)
            {
                State = Models.State.Error.ToString();
                _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Command>(Command.InvalidUrl));
                return false;
            }

            State = Models.State.Ok.ToString();
            _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Command>(Command.ValidUrl));
            return true;
        }

        private void NotifyAll()
        {
            NotifyOfPropertyChange(() => CanStart);
            NotifyOfPropertyChange(() => CanStop);
            NotifyOfPropertyChange(() => CanReset);
        }

        private void Publish(Command command)
        {
            _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Command>(command));
            _eventAggregator.PublishOnUIThreadAsync(new Message<IUCLogPanelViewModel, Command>(command));
            if(command == Command.Start)
            {
                //Send this upon start, if url has been validated and first test ping was successfull
                var testPingResponse = new Response
                {
                    State = Models.State.Ok,
                    Status = HttpStatusCode.OK,
                    DateTime = _dateTimeFactory.GetNow(),
                    Url = Url
                };
                _eventAggregator.PublishOnUIThreadAsync(new Message<IUCLogPanelViewModel, Response>(testPingResponse));
            }
        }

        private void Publish(Exception exception)
        {
            _eventAggregator.PublishOnUIThreadAsync(new Message<IUCLogPanelViewModel, Exception>(exception));
            _eventAggregator.PublishOnUIThreadAsync(new Message<IUCMessageBoxViewModel, Exception>(exception));
        }

        private void Publish(Response response)
        {
            _eventAggregator.PublishOnUIThreadAsync(new Message<IUCLogPanelViewModel, Response>(response));
        }
        #endregion
    }
}
