﻿using Autofac;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using WebWatcher.UI.Factories;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Clients;
using WebWatcher.UI.Services;
using WebWatcher.UI.Validators;
using WebWatcher.UI.ViewModels;
using IContainer = Autofac.IContainer;
using WebWatcher.UI.Models;

namespace WebWatcher.UI
{
    class Startup : BootstrapperBase
    {
        private static IContainer _container;
        public static T Resolve<T>() => _container.Resolve<T>();

        public Startup()
        {
            Initialize();
            RegisterTypes();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<RootViewModel>();
        }

        private void RegisterTypes()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
                //  must be a type that ends with ViewModel
                .Where(type => type.Name.EndsWith("ViewModel"))
                //  must be in a namespace ending with ViewModels
                .Where(type => !( string.IsNullOrWhiteSpace(type.Namespace) ) && ( type.Namespace.EndsWith("ViewModels")))
                //  must implement INotifyPropertyChanged (deriving from PropertyChangedBase will statisfy this)
                .Where(type => type.GetInterface(typeof(INotifyPropertyChanged).Name) != null)
                //  registered as self
                .AsSelf()
                //  always create a new one
                .InstancePerDependency();

            //  register views
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
                //  must be a type that ends with View
                .Where(type => type.Name.EndsWith("View"))
                //  must be in a namespace that ends in Views
                .Where(type => !( string.IsNullOrWhiteSpace(type.Namespace) ) && ( type.Namespace.EndsWith("Views")))
                //  registered as self
                .AsSelf()
                //  always create a new one
                .InstancePerDependency();

            //  register the single window manager for this container
            builder.Register<IWindowManager>(c => new WindowManager()).InstancePerLifetimeScope();
            //  register the single event aggregator for this container
            builder.Register<IEventAggregatorAdapter>(c => new EventAggregatorAdapter(new EventAggregator())).InstancePerLifetimeScope();

            //IO
            builder.RegisterType<CustomWebClient>().As<IWebClient>();
            builder.RegisterType<WebRequestor>().As<IWebRequestor>();

            //Validators
            builder.RegisterType<UrlValidator>().As<IUrlValidator>();

            //Services
            builder.RegisterType<WebService>().As<IWebService>();
            builder.RegisterType<MessageService>().As<IMessageService>();
            builder.RegisterType<TimerService>().As<ITimerService>();

            //Factories
            builder.RegisterType<TimerFactory>().As<ITimerFactory>();
            builder.RegisterType<DateTimeFactory>().As<IDateTimeFactory>();

            //ViewModels
            builder.RegisterType<UCLogPanelViewModel>().As<IUCLogPanelViewModel>();
            builder.RegisterType<UCMessageBoxViewModel>().As<IUCMessageBoxViewModel>();
            builder.RegisterType<UCControlPanelViewModel>().As<IUCControlPanelViewModel>();

            _container = builder.Build();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                if (_container.IsRegistered(serviceType))
                    return _container.Resolve(serviceType);
            }
            else
            {
                if (_container.IsRegisteredWithKey(key, serviceType))
                {
                    return _container.ResolveKeyed(key, serviceType);
                }
            }
            throw new Exception(string.Format("Could not locate any instances of contract {0}.", key ?? serviceType.Name));
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.Resolve(typeof(IEnumerable<>).MakeGenericType(serviceType)) as IEnumerable<object>;
        }

        protected override void BuildUp(object instance)
        {
            _container.InjectProperties(instance);
        }

        private void OpenWindow<T>()
        {
            var manager = _container.Resolve<IWindowManager>();
            manager.ShowWindowAsync(_container.Resolve<T>());
        }
    }
}