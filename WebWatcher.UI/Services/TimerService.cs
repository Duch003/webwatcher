﻿using System.Timers;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Services
{
    public class TimerService : ITimerService
    {
        private ITimerAdapter _timer;
        private ElapsedEventHandler _eventHanlder;
        private ITimerFactory _timerFactory;

        public bool Enabled
        {
            get { return _timer.Enabled; }
        }

        public TimerService(ITimerFactory timerFactory)
        {
            _timerFactory = timerFactory;
            SetupTimer();
        }

        public void Start(double interval)
        {
            _timer.Interval = interval;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Reset()
        {
            _timer.Stop();
            SetupTimer();
        }

        public void AddEventHandler(ElapsedEventHandler elapsedEventHandler)
        {
            _timer.Elapsed += elapsedEventHandler;
            _eventHanlder += elapsedEventHandler;
        }

        public void RemoveEventHandler(ElapsedEventHandler elapsedEventHandler)
        {
            _timer.Elapsed -= elapsedEventHandler;
            _eventHanlder -= elapsedEventHandler;
        }

        private void SetupTimer()
        {
            _timer = _timerFactory.GetTimer();
            _timer.AutoReset = true;
            if (_eventHanlder != null)
            {
                _timer.Elapsed += _eventHanlder;
            }
        }
    }
}
