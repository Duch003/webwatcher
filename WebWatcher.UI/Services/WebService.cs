﻿using System;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Services
{
    public class WebService : IWebService
    {
        protected IWebClient _webClient;
        protected IUrlValidator _validator;

        public WebService(IWebClient webClient, IUrlValidator validator)
        {
            _webClient = webClient;
            _validator = validator;
        }

        public async Task<Result<Response>> CheckPageAsync(string url)
        {
            if (!_validator.IsUrlValid(url))
            {
                var message = $"Url <<{url}>> is invalid.";
                return new Result<Response>(null, new ArgumentException(message, nameof(url)));
            }

            try
            {
                var response = await _webClient.GetAsync(url);
                return new Result<Response>(response);
            }
            catch (Exception e)
            {
                return new Result<Response>(null, e);
            }
        }
    }
}
