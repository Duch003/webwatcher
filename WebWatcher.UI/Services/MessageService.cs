﻿using System;
using System.Collections.Generic;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;

namespace WebWatcher.UI.Services
{
    public class MessageService : IMessageService
    {
        protected readonly string _schemaForResponse;
        protected readonly string _schemaForCommand;
        protected readonly string _schemaForException;
        protected IDateTimeFactory _dateTimeService;
        protected Dictionary<string, int> _colors;
        public MessageService(IDateTimeFactory dateTimeService)
        {
            _colors = new Dictionary<string, int>();
            _colors.Add("green", 2);
            _colors.Add("red", 3);
            _colors.Add("yellow", 4);

            _schemaForResponse = @"{\rtf1\pc" +
                    @"{\colortbl;\red255\green255\blue255;\red0\green255\blue0;\red255\green0\blue0;\red255\green255\blue0;}" +
                    @"\cf1[{{TIME}}] " +
                    @"\cf{{COLORNUM}} \b [{{STATUS}}] \b0" +
                    @"\cf1 {{URL}} \cf2(GET) " +
                    @"\cf1\par}";

            _schemaForCommand = @"{\rtf1\pc" +
                    @"{\colortbl;\red255\green255\blue255;\red0\green255\blue0;\red255\green0\blue0;\red255\green255\blue0;}" +
                    @"\cf1[{{TIME}}] " +
                    @"\cf1 \b [{{COMMAND}}] \b0 " +
                    @"\cf1 {{MESSAGE}}" +
                    @"\par}";

            _schemaForException = @"{\rtf1\pc" +
                    @"{\colortbl;\red255\green255\blue255;\red0\green255\blue0;\red255\green0\blue0;\red255\green255\blue0;}" +
                    @"\cf1[{{TIME}}] " +
                    @"\cf3 \b [Error] \b0 " +
                    @"\cf1 {{MESSAGE}}" +
                    @"\par}";
            _dateTimeService = dateTimeService;
        }

        public Result<string> ResponseToText(Response response)
        {
            if (response is null)
            {
                var e = new ArgumentNullException(nameof(response));
                return new Result<string>(null, e);
            }

            if(response.Url == null)
            {
                var e = new ArgumentNullException(nameof(Response.Url));
                return new Result<string>(null, e);
            }

            var text = (string)_schemaForResponse.Clone();
            if (!response.Url.EndsWith("/"))
            {
                response.Url += "/";
            }

            text = text.Replace("{{TIME}}", response.DateTime.ToLongTimeString())
                .Replace("{{URL}}", response.Url)
                .Replace("{{STATUS}}", response.Status.ToString());

            text = response.State switch
            {
                State.Ok => text.Replace("{{COLORNUM}}", _colors["green"].ToString()),
                State.Error => text.Replace("{{COLORNUM}}", _colors["red"].ToString()),
                _ => text.Replace("{{COLORNUM}}", _colors["yellow"].ToString()),
            };
            return new Result<string>(text);
        }

        public Result<string> CommandToText(Command command)
        {
            if(command == Command.InvalidUrl
                || command == Command.ValidUrl
                || command == Command.UrlNotFound
                || command == Command.UrlFound)
            {
                return new Result<string>(string.Empty);
            }

            var output = (string)_schemaForCommand.Clone();
            output = output
                .Replace("{{TIME}}", _dateTimeService.GetNow().ToLongTimeString())
                .Replace("{{COMMAND}}", $"Timer {command}");

            output = command switch
            {
                Command.Reset => output.Replace("{{MESSAGE}}", "---Timer resetted---"),
                Command.Start => output.Replace("{{MESSAGE}}", "---Timer on---"),
                Command.Stop => output.Replace("{{MESSAGE}}", "---Timer off---"),
                _ => string.Empty,
            };

            return new Result<string>(output);
        }

        public Result<string> ExceptionToText(Exception exception)
        {
            if(exception == null)
            {
                var e = new ArgumentNullException(nameof(exception));
                return new Result<string>(null, e);
            }

            var output = (string)_schemaForException.Clone();
            output = output
                .Replace("{{TIME}}", _dateTimeService.GetNow().ToLongTimeString())
                .Replace("{{MESSAGE}}", exception.Message);

            return new Result<string>(output);
        }
    }
}
