﻿using NUnit.Framework;
using WebWatcher.UI.Validators;

namespace WebWatcher.UI.Tests.Validators
{
    [TestFixture]
    public class UrlValidatorTests
    {
        private UrlValidator _validator;

        #region Tests
        [Test]
        public void IsUrlValid_FullHttpsUrlProvided_ReturnsTrue()
        {
            //Arragnge
            var url = "https://www.anydomain.pl";

            //Act
            var result = _validator.IsUrlValid(url);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsUrlValid_FullHttpUrlProvided_ReturnsTrue()
        {
            //Arragnge
            var url = "http://www.anydomain.pl";

            //Act
            var result = _validator.IsUrlValid(url);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsUrlValid_UrlLacksHttpPrefix_ReturnsFalse()
        {
            //Arragnge
            var url = "www.anydomain.pl";

            //Act
            var result = _validator.IsUrlValid(url);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsUrlValid_ParameterIsEmpty_ReturnsFalse()
        {
            //Arragnge
            var url = "";

            //Act
            var result = _validator.IsUrlValid(url);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsUrlValid_ParameterIsNull_ReturnsFalse()
        {
            //Arragnge
            string url = null;

            //Act
            var result = _validator.IsUrlValid(url);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsUrlValid_NoDomainAndPostfixProvided_ReturnsFalse()
        {
            //Arragnge
            var url = "https://www";

            //Act
            var result = _validator.IsUrlValid(url);

            //Assert
            Assert.IsFalse(result);
        }
        #endregion

        #region Setup
        [OneTimeSetUp]
        public void Setup()
        {
            _validator = new UrlValidator();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _validator = null;
        }
        #endregion
    }
}
