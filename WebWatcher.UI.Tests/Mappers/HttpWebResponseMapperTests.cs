﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Mappers;
using WebWatcher.UI.Models;
using WebWatcher.UI.Tests.Mocks;

namespace WebWatcher.UI.Tests.Mappers
{
    public class HttpWebResponseMapperTests
    {
        private IDateTimeFactory _dateTimeFactory;
        private DateTime _now;

        #region Tests
        [Test]
        [TestCaseSource(nameof(SuccessfullStatusCodes), Category = "Existing successfull responses")]
        public void MapToResponse_ResponseIsSuccessfullAndCodeExistInEnum_ReturnsResponse(HttpStatusCode code)
        {
            //Arrange
            var url = "https://valid_url/";
            var response = new HttpWebResponseMock(code, new Uri(url));

            //Act
            var result = response.MapToResponse(_dateTimeFactory);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Ok);
            Assert.AreEqual(result.Status, code);
            Assert.AreEqual(result.Url, url);
        }

        [Test]
        [TestCaseSource(nameof(FailureStatusCodes), Category = "Existing failure responses")]
        public void MapToResponse_ResponseIsFailureAndCodeExistInEnum_ReturnsResponse(HttpStatusCode code)
        {
            //Arrange
            var url = "https://valid_url/";
            var response = new HttpWebResponseMock(code, new Uri(url));

            //Act
            var result = response.MapToResponse(_dateTimeFactory);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Error);
            Assert.AreEqual(result.Status, code);
            Assert.AreEqual(result.Url, url);
        }

        [Test]
        [TestCaseSource(nameof(RemainingStatusCodes), Category = "Existing non-successfull and non-failure responses")]
        public void MapToResponse_ResponseIsNeitherSuccessfullNorFailureAndCodeExistInEnum_ReturnsResponse(HttpStatusCode code)
        {
            //Arrange
            var url = "https://valid_url/";
            var response = new HttpWebResponseMock(code, new Uri(url));

            //Act
            var result = response.MapToResponse(_dateTimeFactory);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Warning);
            Assert.AreEqual(result.Status, code);
            Assert.AreEqual(result.Url, url);
        }

        [Test]
        public void MapToResponse_ResponseCodeNotDefinedInEnum_ReturnsResponse()
        {
            //Arrange
            var nonexistingStatusCode = (HttpStatusCode)1;
            var url = "https://valid_url/";
            var response = new HttpWebResponseMock(nonexistingStatusCode, new Uri(url));

            //Act
            var result = response.MapToResponse(_dateTimeFactory);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Warning);
            Assert.AreEqual(result.Status, nonexistingStatusCode);
            Assert.AreEqual(result.Url, url);
        }

        [Test]
        public void MapToResponse_ResponseUriIsNull_ThrowsNullReferenceException()
        {
            //Arrange
            var response = new HttpWebResponseMock(HttpStatusCode.OK, null);

            //Act
            //Assert
            Assert.Throws<NullReferenceException>(() => response.MapToResponse(_dateTimeFactory));
        }

        [Test]
        public void MapToResponse_ResponseIsNull_ThrowsNullReferenceException()
        {
            //Arrange
            HttpWebResponseMock response = null;

            //Act
            //Assert
            Assert.Throws<NullReferenceException>(() => response.MapToResponse(_dateTimeFactory));
        }

        [Test]
        public void MapToResponse_DateTimeFactoryIsNull_ThrowsArgumentNullException()
        {
            //Arrange
            var url = "https://valid_url/";
            var response = new HttpWebResponseMock(HttpStatusCode.OK, new Uri(url));

            //Act
            //Assert
            Assert.Throws<ArgumentNullException>(() => response.MapToResponse(null));
        }
        #endregion

        #region Test case sources
        [SetUp]
        public void Setup()
        {
            _now = new DateTime(2021, 10, 11);
            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock.Setup(x => x.GetNow()).Returns(_now);
            _dateTimeFactory = dateTimeFactoryMock.Object;
        }

        private static IEnumerable<HttpStatusCode> SuccessfullStatusCodes()
        {
            return Enum
                .GetValues(typeof(HttpStatusCode))
                .Cast<HttpStatusCode>()
                .Where(x => (int)x >= 200 && (int)x < 300)
                .ToList();
        }

        private static IEnumerable<HttpStatusCode> FailureStatusCodes()
        {
            return Enum
                .GetValues(typeof(HttpStatusCode))
                .Cast<HttpStatusCode>()
                .Where(x => (int)x >= 400 && (int)x < 600)
                .ToList();
        }

        private static IEnumerable<HttpStatusCode> RemainingStatusCodes()
        {
            return Enum
                .GetValues(typeof(HttpStatusCode))
                .Cast<HttpStatusCode>()
                .Where(x => !(((int)x >= 400 && (int)x < 600) || ((int)x >= 200 && (int)x < 300)))
                .ToList();
        }
        #endregion
    }
}
