﻿using Moq;
using NUnit.Framework;
using System;
using System.Net;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;
using WebWatcher.UI.Services;

namespace WebWatcher.UI.Tests.Services
{
    [TestFixture]
    public class WebServiceTests
    {
        private string _okUrl = "validUrl";
        private string _invalidUrl = "invalidUrl";
        private string _warningUrl = "warningUrl";
        private string _errorUrl = "errorUrl";
        private string _noInternetConnectionError = "no internet connection";
        private string _whitespaceUrl = " ";
        private Response _ok;
        private Response _warning;
        private Response _error;
        private WebService _service;

        #region Tests
        [Test]
        public async Task CheckPageAsync_UrlIsValidAndReturnsOk_ReturnsResponseWithSuccess()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(_okUrl);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.IsNotNull(result.Output);
            Assert.AreEqual(result.Output, _ok);
        }

        [Test]
        public async Task CheckPageAsync_UrlIsValidAndReturnsAmbiguous_ReturnsResponseWithSuccess()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(_warningUrl);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.IsNotNull(result.Output);
            Assert.AreEqual(result.Output, _warning);
        }

        [Test]
        public async Task CheckPageAsync_UrlIsValidAndReturnsNotFound_ReturnsResponseWithSuccess()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(_errorUrl);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.IsNotNull(result.Output);
            Assert.AreEqual(result.Output, _error);
        }

        [Test]
        public async Task CheckPageAsync_UrlIsEmpty_ReturnsResponseWithException()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(string.Empty);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<ArgumentException>(result.Exception);
            Assert.AreEqual(result.Exception.Message, $"Url <<>> is invalid.{Environment.NewLine}Parameter name: url");
        }

        [Test]
        public async Task CheckPageAsync_UrlIsNull_ReturnsResponseWithException()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(null);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<ArgumentException>(result.Exception);
            Assert.AreEqual(result.Exception.Message, $"Url <<>> is invalid.{Environment.NewLine}Parameter name: url");
        }

        [Test]
        public async Task CheckPageAsync_UrlIsWhitespace_ReturnsResponseWithException()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(_whitespaceUrl);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<ArgumentException>(result.Exception);
            Assert.AreEqual(result.Exception.Message, $"Url << >> is invalid.{Environment.NewLine}Parameter name: url");
        }

        [Test]
        public async Task CheckPageAsync_UrlIsInvalid_ReturnsResponseWithException()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(_invalidUrl);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<ArgumentException>(result.Exception);
            Assert.AreEqual(result.Exception.Message, $"Url <<{_invalidUrl}>> is invalid.{Environment.NewLine}Parameter name: url");
        }

        [Test]
        public async Task CheckPageAsync_UrlIsValidButErrorOccursInTheClient_ReturnsResponseWithException()
        {
            //Arrange
            //Act
            var result = await _service.CheckPageAsync(_noInternetConnectionError);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<Exception>(result.Exception);
            Assert.AreEqual(result.Exception.Message, "Custom exception");
        }
        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            _ok = new Response
            {
                State = State.Ok,
                Status = HttpStatusCode.OK,
                DateTime = new DateTime(2021, 10, 11),
                Url = _okUrl
            };
            _warning = new Response
            {
                State = State.Warning,
                Status = HttpStatusCode.Ambiguous,
                DateTime = new DateTime(2021, 7, 2),
                Url = _warningUrl
            };
            _error = new Response
            {
                State = State.Error,
                Status = HttpStatusCode.NotFound,
                DateTime = new DateTime(2021, 1, 1),
                Url = _errorUrl
            };

            var webClientMock = new Mock<IWebClient>();
            webClientMock.Setup(x => x.GetAsync(_okUrl)).Returns(Task.FromResult(_ok));
            webClientMock.Setup(x => x.GetAsync(_warningUrl)).Returns(Task.FromResult(_warning));
            webClientMock.Setup(x => x.GetAsync(_errorUrl)).Returns(Task.FromResult(_error));
            webClientMock.Setup(x => x.GetAsync(_noInternetConnectionError)).Throws(new Exception("Custom exception"));

            var urlValidatiorMock = new Mock<IUrlValidator>();
            urlValidatiorMock.Setup(x => x.IsUrlValid(_okUrl)).Returns(true);
            urlValidatiorMock.Setup(x => x.IsUrlValid(_noInternetConnectionError)).Returns(true);
            urlValidatiorMock.Setup(x => x.IsUrlValid(_warningUrl)).Returns(true);
            urlValidatiorMock.Setup(x => x.IsUrlValid(_errorUrl)).Returns(true);
            urlValidatiorMock.Setup(x => x.IsUrlValid(_invalidUrl)).Returns(false);
            urlValidatiorMock.Setup(x => x.IsUrlValid(_whitespaceUrl)).Returns(false);
            urlValidatiorMock.Setup(x => x.IsUrlValid(string.Empty)).Returns(false);
            urlValidatiorMock.Setup(x => x.IsUrlValid(null)).Returns(false);

            _service = new WebService(webClientMock.Object, urlValidatiorMock.Object);
        }

        public void TearDown()
        {
            _ok = null;
            _warning = null;
            _error = null;
            _service = null;
        }
        #endregion
    }
}
