﻿using Moq;
using NUnit.Framework;
using System;
using System.Threading;
using System.Timers;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Services;
using WebWatcher.UI.Tests.Mocks;

namespace WebWatcher.UI.Tests.Services
{
    [TestFixture]
    public class TimerServiceTests
    {
        private TimerService _timerService;
        private ITimerAdapter _timer;
        private TimerMock _timerMock => _timer as TimerMock;

        #region Tests
        [Test]
        public void Start_ParameterIsIntMaxValue_StartsTheTimerWithGivenTime()
        {
            //Arrange
            var time = (double)int.MaxValue;

            //Act
            _timerService.Start(time);

            //Assert
            Assert.IsTrue(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == time);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 0);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }

        [Test]
        public void Start_ParameterIsZero_ThrowsArgumentException()
        {
            //Arrange
            var time = 0d;

            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => _timerService.Start(time));
        }

        [Test]
        public void Start_ParameterIsNegative_ThrowsArgumentException()
        {
            //Arrange
            var time = -1d;

            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => _timerService.Start(time));
        }

        [Test]
        public void Start_ParameterIsNaN_ThrowsArgumentOutOfRangeException()
        {
            //Arrange
            var time = double.NaN;

            //Act
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => _timerService.Start(time));
        }

        [Test]
        public void Start_ParameterIsGreaterThanIntMaxValue_ThrowsArgumentOutOfRangeException()
        {
            //Arrange
            var time = ((double)int.MaxValue) + 1;

            //Act
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => _timerService.Start(time));
        }

        [Test]
        public void Start_ParameterIsPositiveInfinity_ThrowsArgumentOutOfRangeException()
        {
            //Arrange
            var time = double.PositiveInfinity;

            //Act
            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => _timerService.Start(time));
        }

        [Test]
        public void Start_ParameterIsNegativeInfinity_ThrowsArgumentException()
        {
            //Arrange
            var time = double.NegativeInfinity;

            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => _timerService.Start(time));
        }

        [Test]
        public void Stop_IfCalled_ThenSetsEnabledToFalse()
        {
            //Arrange
            //Act
            _timerService.Stop();

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 0);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }

        [Test]
        public void Reset_IfCalled_ThenRestoresDefaultValues()
        {
            //Arrange
            _timerMock.AutoReset = false;
            _timerMock.Enabled = true;
            _timerMock.Interval = int.MaxValue;

            //Act
            _timerService.Reset();

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 0);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }

        [Test]
        public void AddEventHandler_ParameterIsValid_AddsEventHandler()
        {
            //Arrange
            ElapsedEventHandler handler = (object obj, ElapsedEventArgs eventArgs) => { };

            //Act
            _timerService.AddEventHandler(handler);

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 1);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }

        [Test]
        public void AddEventHandler_ParameterIsNull_AddsEventHandler()
        {
            //Arrange
            ElapsedEventHandler handler = null;

            //Act
            _timerService.AddEventHandler(handler);

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 1);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }

        [Test]
        public void RemoveEventHandler_ParameterIsValid_RemovesEventHandler()
        {
            //Arrange
            ElapsedEventHandler handler = (object obj, ElapsedEventArgs eventArgs) => { };

            //Act
            _timerService.RemoveEventHandler(handler);

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 0);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 1);
        }

        [Test]
        public void RemoveEventHandler_ParameterIsNull_RemovesEventHandler()
        {
            //Arrange
            ElapsedEventHandler handler = null;

            //Act
            _timerService.RemoveEventHandler(handler);

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 0);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 1);
        }

        [Test]
        public void Start_IfTimeElapsed_ThenFiresEvent()
        {
            //Arrange
            var called = false;
            ElapsedEventHandler handler = (_, __) => called = true;

            //Act
            _timerService.AddEventHandler(handler);
            _timerService.Start(50);
            Thread.Sleep(100);

            //Assert
            Assert.IsTrue(called);
            Assert.IsTrue(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 50);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 1);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }

        [Test]
        public void Reset_HandlerAlreadyAdded_RestoresStateAndReassignsHandler()
        {
            //Arrange
            ElapsedEventHandler handler = (_, __) => { };

            //Act
            _timerService.AddEventHandler(handler);
            _timerService.Reset();

            //Assert
            Assert.IsFalse(_timerService.Enabled);
            Assert.IsTrue(_timer.AutoReset);
            Assert.IsTrue(_timer.Interval == 100);
            Assert.IsTrue(TimerMock.ElapsedAddCalls == 2);
            Assert.IsTrue(TimerMock.ElapsedRemoveCalls == 0);
        }
        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            TimerMock.ResetCounters();
            var mockTimerFactory = new Mock<ITimerFactory>();
            mockTimerFactory.Setup(x => x.GetTimer()).Returns(() => 
            {
                _timer = new TimerMock();
                return _timer;
            });

            _timerService = new TimerService(mockTimerFactory.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _timerService = null;
            _timer = null;
        }
        #endregion
    }
}
