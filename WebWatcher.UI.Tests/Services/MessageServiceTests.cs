﻿using Moq;
using NUnit.Framework;
using System;
using System.Net;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;
using WebWatcher.UI.Services;

namespace WebWatcher.UI.Tests.Services
{
    [TestFixture]
    public class MessageServiceTests
    {
        private DateTime _dateTime;
        private MessageService _service;
        private readonly string _schemaForResponse = @"{\rtf1\pc" +
        //Definition of colors:  1: White                 2: Green             3: Red               4: Yellow
                    @"{\colortbl;\red255\green255\blue255;\red0\green255\blue0;\red255\green0\blue0;\red255\green255\blue0;}" +
                    @"\cf1[{{TIME}}] " +
                    @"\cf{{COLORNUM}} \b [{{STATUS}}] \b0" +
                    @"\cf1 {{URL}} \cf2(GET) " +
                    @"\cf1\par}";
        private readonly string _schemaForCommand = @"{\rtf1\pc" +
        //Definition of colors:  1: White                 2: Green             3: Red               4: Yellow
                    @"{\colortbl;\red255\green255\blue255;\red0\green255\blue0;\red255\green0\blue0;\red255\green255\blue0;}" +
                    @"\cf1[{{TIME}}] " +
                    @"\cf1 \b [{{COMMAND}}] \b0 " +
                    @"\cf1 {{MESSAGE}}" +
                    @"\par}";
        private readonly string _schemaForException = @"{\rtf1\pc" +
        //Definition of colors:  1: White                 2: Green             3: Red               4: Yellow
                    @"{\colortbl;\red255\green255\blue255;\red0\green255\blue0;\red255\green0\blue0;\red255\green255\blue0;}" +
                    @"\cf1[{{TIME}}] " +
                    @"\cf3 \b [Error] \b0 " +
                    @"\cf1 {{MESSAGE}}" +
                    @"\par}";
        /* 
         * LEGEND:
         * \cf{id} {text}   -> following text has selected (id -> 1 - 4) color
         * \b {text} \b0    -> the text between tags is bold
         * \rtf1\pc         -> start of the rich text
         * \par             -> end of the rich text
         */

        #region Tests
        [Test]
        public void ResponseToText_StateIsOk_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "testUrl/";
            var responseStatus = HttpStatusCode.OK;
            var responseState = State.Ok;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", responseUrl)
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "2");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_StateIsWarning_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "testUrl/";
            var responseStatus = HttpStatusCode.OK;
            var responseState = State.Warning;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", responseUrl)
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "4");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_StateIsError_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "testUrl/";
            var responseStatus = HttpStatusCode.OK;
            var responseState = State.Error;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", responseUrl)
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "3");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_StateDoesNotExist_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "testUrl/";
            var responseStatus = HttpStatusCode.OK;
            var responseState = (State)100;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", responseUrl)
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "4");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_StatusIsNonAuthoritativeInformation_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "testUrl/";
            var responseStatus = HttpStatusCode.NonAuthoritativeInformation;
            var responseState = State.Ok;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", responseUrl)
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "2");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_StatusDoesNotExist_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "testUrl/";
            var responseStatus = (HttpStatusCode)900;
            var responseState = State.Ok;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", responseUrl)
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "2");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_UrlIsEmpty_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = string.Empty;
            var responseStatus = HttpStatusCode.OK;
            var responseState = State.Ok;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", "/")
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "2");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_UrlIsNull_ReturnsResultWithException()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            string responseUrl = null;
            var responseStatus = HttpStatusCode.OK;
            var responseState = State.Ok;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<ArgumentNullException>(result.Exception);
            Assert.AreEqual(result.Exception.Message, $"Value cannot be null.{Environment.NewLine}Parameter name: {nameof(Response.Url)}");
        }

        [Test]
        public void ResponseToText_UrlDoesNotEndWithSlash_ReturnsResultWithOutput()
        {
            //Arrage
            var responseDate = new DateTime(2021, 1, 1);
            var responseUrl = "nodash";
            var responseStatus = HttpStatusCode.OK;
            var responseState = State.Ok;
            var response = new Response
            {
                Url = responseUrl,
                DateTime = responseDate,
                Status = responseStatus,
                State = responseState
            };
            var expectedOutput = _schemaForResponse
                //DateTime - Response time
                .Replace("{{TIME}}", responseDate.ToLongTimeString())
                //Url - Response url
                .Replace("{{URL}}", "nodash/")
                //Status - HTTP status code
                .Replace("{{STATUS}}", responseStatus.ToString())
                //State - Response state
                .Replace("{{COLORNUM}}", "2");

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ResponseToText_ParameterIsNull_ReturnsResultWithException()
        {
            //Arrage
            Response response = null;

            //Act
            var result = _service.ResponseToText(response);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.IsInstanceOf<ArgumentNullException>(result.Exception);
            Assert.AreEqual(result.Exception.Message, $"Value cannot be null.{Environment.NewLine}Parameter name: response");
        }

        [Test]
        public void CommandToText_CommandIsStart_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.Start;
            var expectedOutput = _schemaForCommand
                //DateTime - Current datetime
                .Replace("{{TIME}}", _dateTime.ToLongTimeString())
                //Command - Command enum
                .Replace("{{COMMAND}}", $"Timer {command}")
                //Status - HTTP status code
                .Replace("{{MESSAGE}}", "---Timer on---");

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandIsStop_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.Stop;
            var expectedOutput = _schemaForCommand
                //DateTime - Current datetime
                .Replace("{{TIME}}", _dateTime.ToLongTimeString())
                //Command - Command enum
                .Replace("{{COMMAND}}", $"Timer {command}")
                //Status - HTTP status code
                .Replace("{{MESSAGE}}", "---Timer off---");

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandIsReset_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.Reset;
            var expectedOutput = _schemaForCommand
                //DateTime - Current datetime
                .Replace("{{TIME}}", _dateTime.ToLongTimeString())
                //Command - Command enum
                .Replace("{{COMMAND}}", $"Timer {command}")
                //Status - HTTP status code
                .Replace("{{MESSAGE}}", "---Timer resetted---");

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandIsInvalidUrl_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.InvalidUrl;
            var expectedOutput = string.Empty;

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandIsValidUrl_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.ValidUrl;
            var expectedOutput = string.Empty;

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandIsUrlNotFound_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.UrlNotFound;
            var expectedOutput = string.Empty;

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandIsUrlFound_ReturnsResultWithOutput()
        {
            //Arrage
            var command = Command.UrlFound;
            var expectedOutput = string.Empty;

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void CommandToText_CommandDoesNotExist_ReturnsResultWithOutput()
        {
            //Arrage
            var command = (Command)100;
            var expectedOutput = string.Empty;

            //Act
            var result = _service.CommandToText(command);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ExceptionToText_ParameterIsValid_ReturnsResultWithOutput()
        {
            //Arrage
            var exception = new Exception("Test exception");
            var expectedOutput = _schemaForException
                //DateTime - Response time
                .Replace("{{TIME}}", _dateTime.ToLongTimeString())
                //Message - Exception message
                .Replace("{{MESSAGE}}", exception.Message);

            //Act
            var result = _service.ExceptionToText(exception);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ExceptionToText_ExceptionMessageIsNull_ReturnsResultWithOutput()
        {
            //Arrage
            var exception = new Exception(null);
            var expectedOutput = _schemaForException
                //DateTime - Response time
                .Replace("{{TIME}}", _dateTime.ToLongTimeString())
                //Message - Exception message
                .Replace("{{MESSAGE}}", exception.Message);

            //Act
            var result = _service.ExceptionToText(exception);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ExceptionToText_ExceptionMessageIsEmpty_ReturnsResultWithOutput()
        {
            //Arrage
            var exception = new Exception(string.Empty);
            var expectedOutput = _schemaForException
                //DateTime - Response time
                .Replace("{{TIME}}", _dateTime.ToLongTimeString())
                //Message - Exception message
                .Replace("{{MESSAGE}}", exception.Message);

            //Act
            var result = _service.ExceptionToText(exception);

            //Assert
            Assert.IsTrue(result.IsFine);
            Assert.IsNull(result.Exception);
            Assert.AreEqual(expectedOutput, result.Output);
        }

        [Test]
        public void ExceptionToText_ParameterIsNull_ReturnsResultWithOutput()
        {
            //Arrage
            Exception exception = null;

            //Act
            var result = _service.ExceptionToText(exception);

            //Assert
            Assert.IsFalse(result.IsFine);
            Assert.IsNotNull(result.Exception);
            Assert.IsNull(result.Output);
            Assert.AreEqual(result.Exception.Message, $"Value cannot be null.{Environment.NewLine}Parameter name: exception");
        }

        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            _dateTime = new DateTime(2021, 10, 11);

            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock.Setup(x => x.GetNow()).Returns(_dateTime);

            _service = new MessageService(dateTimeFactoryMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _service = null;
        }
        #endregion
    }
}
