﻿using System.Timers;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Tests.Mocks
{
    class TimerMock : ITimerAdapter
    {
        private Timer _timer = new Timer();

        public static int ElapsedAddCalls { get; private set; } = 0;
        public static int ElapsedRemoveCalls { get; private set; } = 0;

        public static void ResetCounters()
        {
            ElapsedAddCalls = 0;
            ElapsedRemoveCalls = 0;
        }

        public bool AutoReset
        {
            get
            {
                return _timer.AutoReset;
            }
            set
            {
                _timer.AutoReset = value;
            }
        }
        public bool Enabled
        {
            get
            {
                return _timer.Enabled;
            }
            set
            {
                _timer.Enabled = value;
            }
        }
        public double Interval
        {
            get
            {
                return _timer.Interval;
            }
            set
            {
                _timer.Interval = value;
            }
        }

        public event ElapsedEventHandler Elapsed
        {
            add
            {
                ElapsedAddCalls++;
                _timer.Elapsed += value;
            }
            remove
            {
                ElapsedRemoveCalls++;
                _timer.Elapsed += value;
            }
        }

        //https://docs.microsoft.com/en-us/dotnet/api/system.timers.timer.interval?view=net-5.0
        public void Start()
        {
            _timer.Start();
        }
        public void Stop()
        {
            _timer.Stop();
        }
    }
}
