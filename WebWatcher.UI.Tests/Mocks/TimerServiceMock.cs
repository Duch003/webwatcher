﻿using System.Timers;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Tests.Mocks
{
    public class TimerServiceMock : ITimerService
    {
        private bool _enabled;
        public bool Enabled 
        {
            get
            {
                return _enabled;
            }
            private set
            {
                _enabled = value;
            }
        }

        private int _addEventHandlerCalls;
        public int AddEventHandlerCalls
        {
            get
            {
                return _addEventHandlerCalls;
            }
            private set
            {
                _addEventHandlerCalls = value;
            }
        }

        private int _removeEventHandlerCalls;
        public int RemoveEventHandlerCalls
        {
            get
            {
                return _removeEventHandlerCalls;
            }
            private set
            {
                _removeEventHandlerCalls = value;
            }
        }

        private ElapsedEventHandler _eventHandler;

        public double Interval { get; set; }

        public void AddEventHandler(ElapsedEventHandler elapsedEventHandler)
        {
            _eventHandler += elapsedEventHandler;
            AddEventHandlerCalls++;
        }
        public void RemoveEventHandler(ElapsedEventHandler elapsedEventHandler)
        {
            _eventHandler -= elapsedEventHandler;
            RemoveEventHandlerCalls++;
        }
        public void Reset() => Enabled = false;
        public void Start(double interval)
        {
            Enabled = true;
            Interval = interval;
        }
        public void Stop() => Enabled = false;
        public void Elapsed()
        { 
            if(_eventHandler != null)
            {
                _eventHandler.Invoke(null, null);
            }
        }
    }
}
