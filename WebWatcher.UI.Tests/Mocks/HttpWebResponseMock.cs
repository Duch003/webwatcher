﻿using System;
using System.Net;
using WebWatcher.UI.Interfaces;

namespace WebWatcher.UI.Tests.Mocks
{
    public class HttpWebResponseMock : IHttpWebResponseAdapter
    {
        public HttpWebResponseMock(HttpStatusCode statusCode, Uri responseUri)
        {
            StatusCode = statusCode;
            ResponseUri = responseUri;
        }

        private HttpStatusCode _statusCode;

        public HttpStatusCode StatusCode
        {
            get
            {
                return _statusCode;
            }
            private set
            {
                _statusCode = value;
            }
        }

        private Uri _responseUri;

        public Uri ResponseUri
        {
            get
            {
                return _responseUri;
            }
            private set
            {
                _responseUri = value;
            }
        }
    }
}
