﻿using Moq;
using NUnit.Framework;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;
using WebWatcher.UI.ViewModels;

namespace WebWatcher.UI.Tests.ViewModels
{
    [TestFixture]
    public class UCMessageBoxViewModelTests
    {
        private UCMessageBoxViewModel _viewModel;

        private bool _viewModelSubscribes;
        private DateTime _now;

        #region Tests
        [Test]
        public void Constructor_InitialStateCheck()
        {
            //Arrange
            //Act
            //Assert
            Assert.IsTrue(_viewModel.State == State.Ok.ToString());
            Assert.IsTrue(_viewModel.Message == "Ready to go!");
            Assert.IsTrue(_viewModelSubscribes);
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsOk_ChangesMessageAndState()
        {
            //Arrange
            var response = new Response
            {
                State = State.Ok,
                Status = HttpStatusCode.OK,
                DateTime = _now,
                Url = "Does not matter"
            };
            var message = new Message<IUCMessageBoxViewModel, Response>(response);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == response.State.ToString());
            Assert.IsTrue(_viewModel.Message == HttpWorkerRequest.GetStatusDescription((int)response.Status));
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsAmbiguous_ChangesMessageAndState()
        {
            //Arrange
            var response = new Response
            {
                State = State.Warning,
                Status = HttpStatusCode.Ambiguous,
                DateTime = _now,
                Url = "Does not matter"
            };
            var message = new Message<IUCMessageBoxViewModel, Response>(response);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == response.State.ToString());
            Assert.IsTrue(_viewModel.Message == HttpWorkerRequest.GetStatusDescription((int)response.Status));
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsNotFound_ChangesMessageAndState()
        {
            //Arrange
            var response = new Response
            {
                State = State.Error,
                Status = HttpStatusCode.NotFound,
                DateTime = _now,
                Url = "Does not matter"
            };
            var message = new Message<IUCMessageBoxViewModel, Response>(response);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == response.State.ToString());
            Assert.IsTrue(_viewModel.Message == HttpWorkerRequest.GetStatusDescription((int)response.Status));
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsNull_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Response>(null);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == "Error: Response was null.");
        }

        [Test]
        public void HandleAsyncResponse_MessageIsNull_ThrowsNullReferenceException()
        {
            //Arrange
            Message<IUCMessageBoxViewModel, Response> message = null;
            var cancellationToken = default(CancellationToken);

            //Act
            //Assert
            Assert.ThrowsAsync<NullReferenceException>(() => _viewModel.HandleAsync(message, cancellationToken));
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsStart_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Command>(Command.Start);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Warning.ToString());
            Assert.IsTrue(_viewModel.Message == "Running...");
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsStop_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Command>(Command.Stop);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Warning.ToString());
            Assert.IsTrue(_viewModel.Message == "Ready to resume.");
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsValidUrl_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Command>(Command.ValidUrl);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Ok.ToString());
            Assert.IsTrue(_viewModel.Message == "Ready to go!");
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsReset_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Command>(Command.Reset);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Ok.ToString());
            Assert.IsTrue(_viewModel.Message == "Ready to go!");
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsInvalidUrl_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Command>(Command.InvalidUrl);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == "Url is invalid.");
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsUrlNotFound_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Command>(Command.UrlNotFound);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == "It seems like given url does not exist.");
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsNotDefined_ChangesMessageAndState()
        {
            //Arrange
            var notDefinedCommand = (Command)90;
            var message = new Message<IUCMessageBoxViewModel, Command>(notDefinedCommand);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == $"Unknown command received: {notDefinedCommand}");
        }

        [Test]
        public async Task HandleAsyncException_ExceptionIsValid_ChangesMessageAndState()
        {
            //Arrange
            var errorMessage = "Custom message.";
            var innerErrorMessageLvl1 = "Inner error message lvl 1.";
            var innerErrorMessageLvl2 = "Inner error message lvl 2.";
            var innerErrorMessageLvl3 = "Inner error message lvl 3.";
            var innerErrorMessageLvl4 = "Inner error message lvl 4.";
            var exception = new Exception(errorMessage, 
                new Exception(innerErrorMessageLvl1, 
                    new Exception(innerErrorMessageLvl2, 
                        new Exception(innerErrorMessageLvl3, 
                            new Exception(innerErrorMessageLvl4)))));
            var message = new Message<IUCMessageBoxViewModel, Exception>(exception);
            var cancellationToken = default(CancellationToken);
            var expectedMessage = "MESSAGE:\r\nCustom message." +
                "\r\n\tINNER \tMESSAGE:\r\n\tInner error message lvl 1." +
                "\r\n\t\tINNER \t\tMESSAGE:\r\n\t\tInner error message lvl 2." +
                "\r\n\t\t\tINNER \t\t\tMESSAGE:\r\n\t\t\tInner error message lvl 3." +
                "\r\n\t\t\t\tINNER \t\t\t\tMESSAGE:\r\n\t\t\t\tInner error message lvl 4." +
                "\r\n\r\n\r\n\r\n\r\n";

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == expectedMessage);
        }

        [Test]
        public async Task HandleAsyncException_ExceptionHasNoInnerExceptions_ChangesMessageAndState()
        {
            //Arrange
            var errorMessage = "Custom message.";
            var exception = new Exception(errorMessage);
            var message = new Message<IUCMessageBoxViewModel, Exception>(exception);
            var cancellationToken = default(CancellationToken);
            var expectedMessage = "MESSAGE:\r\nCustom message.\r\n";

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == expectedMessage);
        }

        [Test]
        public async Task HandleAsyncException_ExceptionIsNull_ChangesMessageAndState()
        {
            //Arrange
            var message = new Message<IUCMessageBoxViewModel, Exception>(null);
            var cancellationToken = default(CancellationToken);
            var expectedMessage = "Unknown error.";

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.State == State.Error.ToString());
            Assert.IsTrue(_viewModel.Message == expectedMessage);
        }

        [Test]
        public void HandleAsyncException_MessageIsNull_ThrowsNullReferenceException()
        {
            //Arrange
            Message<IUCMessageBoxViewModel, Exception> message = null;
            var cancellationToken = default(CancellationToken);

            //Act
            //Assert
            Assert.ThrowsAsync<NullReferenceException>(() => _viewModel.HandleAsync(message, cancellationToken));
        }
        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            _now = new DateTime(2021, 10, 15);

            var eventAggregatorMock = new Mock<IEventAggregatorAdapter>();
            eventAggregatorMock.Setup(x => x.SubscribeOnPublishedThread(It.IsAny<object>()))
                .Callback<object>(obj => 
                {
                    _viewModelSubscribes = obj.GetType() == typeof(UCMessageBoxViewModel);
                });

            _viewModel = new UCMessageBoxViewModel(eventAggregatorMock.Object);
        }
        #endregion
    }
}
