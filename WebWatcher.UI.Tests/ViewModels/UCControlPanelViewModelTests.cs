﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;
using WebWatcher.UI.Tests.Mocks;
using WebWatcher.UI.ViewModels;

namespace WebWatcher.UI.Tests.ViewModels
{
    public class UCControlPanelViewModelTests
    {
        private UCControlPanelViewModel _viewModel;
        private TimerServiceMock _timerServiceMock;

        private List<Message<IUCMessageBoxViewModel, Exception>> _messages_IUCMessageBoxViewModel_Exceptions;
        private List<Message<IUCMessageBoxViewModel, Command>> _messages_IUCMessageBoxViewModel_Commands; 
        private List<Message<IUCLogPanelViewModel, Response>> _messages_IUCLogPanelViewModel_Responses;
        private List<Message<IUCLogPanelViewModel, Command>> _messages_IUCLogPanelViewModel_Commands;
        private List<Message<IUCLogPanelViewModel, Exception>> _messages_IUCLogPanelViewModel_Exceptions;

        private string _okUrl = "will return OK 200";
        private string _badRequestUrl = "will return Bad Request 400";
        private string _ambiguousUrl = "will return Ambiguous 300";
        private string _invalidUrl = "url with invalid format";
        private string _errorUrl = "url is valid but no connection can be established - no internet connection or something";

        private DateTime _now;
        private bool _urlValidated;
        private bool _viewModelSubscribes;

        #region Tests
        [Test]
        public void TimeSetter_NewValueIsTen_SetsTen()
        {
            //Arrange
            var newValue = 10d;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, newValue);
        }

        [Test]
        public void TimeSetter_NewValueIsLowerThanTen_SetsTen()
        {
            //Arrange
            var newValue = 9d;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, 10d);
        }

        [Test]
        public void TimeSetter_NewValueIsOneHundredTwenty_SetsOneHundredTwenty()
        {
            //Arrange
            var newValue = 120d;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, newValue);
        }

        [Test]
        public void TimeSetter_NewValueIsGreaterThanOneHundredTwenty_SetsOneHundredTwenty()
        {
            //Arrange
            var newValue = 121d;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, 120d);
        }

        [Test]
        public void TimeSetter_NewValueIsReal_SetsFlooredNumber()
        {
            //Arrange
            var newValue = 49.92d;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, 49d);
        }

        [Test]
        public void TimeSetter_NewValueIsNan_SetsFlooredNumber()
        {
            //Arrange
            var newValue = double.NaN;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, 10);
        }

        [Test]
        public void TimeSetter_NewValueIsPositiveInfinity_SetsFlooredNumber()
        {
            //Arrange
            var newValue = double.PositiveInfinity;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, 120);
        }

        [Test]
        public void TimeSetter_NewValueIsNegativeInfinity_SetsFlooredNumber()
        {
            //Arrange
            var newValue = double.NegativeInfinity;

            //Act
            _viewModel.Time = newValue;

            //Assert
            Assert.AreEqual(_viewModel.Time, 10);
        }

        [Test]
        public void MultipliedTimeGetter_MultipliesSettedTimeByThousand_ReturnsTimeMultipliedByThousand()
        {
            //Arrange
            _viewModel.Time = 30;

            //Act
            var result = _viewModel.MultipliedTime;

            //Assert
            Assert.AreEqual(result, 30*1000);
        }

        [Test]
        public void CanStart_TimerIsDisabledAndUrlIsValid_ReturnsTrue()
        {
            //Arrange
            _viewModel.Url = _okUrl;
            _timerServiceMock.Reset(); //sets Enabled to false

            //Act
            var result = _viewModel.CanStart;

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CanStart_TimerIsDisabledAndUrlIsInvalid_ReturnsFalse()
        {
            //Arrange
            _viewModel.Url = _invalidUrl;
            _timerServiceMock.Reset(); //sets Enabled to false

            //Act
            var result = _viewModel.CanStart;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CanStart_TimerIsEnabledAndUrlIsValid_ReturnsFalse()
        {
            //Arrange
            _viewModel.Url = _okUrl;
            _timerServiceMock.Start(1d); //sets Enabled to true

            //Act
            var result = _viewModel.CanStart;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CanStart_TimerIsEnabledAndUrlIsInvalid_ReturnsFalse()
        {
            //Arrange
            _viewModel.Url = _invalidUrl;
            _timerServiceMock.Start(1d); //sets Enabled to true

            //Act
            var result = _viewModel.CanStart;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CanStop_TimerIsEnabled_ReturnsTrue()
        {
            //Arrange
            _timerServiceMock.Start(1d); //sets Enabled to true

            //Act
            var result = _viewModel.CanStop;

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CanStop_TimerIsDisabled_ReturnsFalse()
        {
            //Arrange
            _timerServiceMock.Stop(); //sets Enabled to false

            //Act
            var result = _viewModel.CanStop;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CanReset_TimerIsDisabled_ReturnsTrue()
        {
            //Arrange
            _timerServiceMock.Stop(); //sets Enabled to false

            //Act
            var result = _viewModel.CanReset;

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CanReset_TimerIsEnabled_ReturnsFalse()
        {
            //Arrange
            _timerServiceMock.Start(1d); //sets Enabled to true

            //Act
            var result = _viewModel.CanReset;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void UrlSetter_IfValidUrlIsBeingSet_ThenCallsIsUrlValidOfUrlValidator()
        {
            //Arrange
            _timerServiceMock.Stop(); //sets Enabled to true

            //Act
            _viewModel.Url = _okUrl;

            //Assert
            Assert.IsTrue(_urlValidated);
            Assert.IsTrue(_viewModel.CanStart);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void UrlSetter_IfInvalidUrlIsBeingSet_ThenCallsIsUrlValidOfUrlValidator()
        {
            //Arrange
            _timerServiceMock.Stop(); //sets Enabled to true

            //Act
            _viewModel.Url = _invalidUrl;

            //Assert
            Assert.IsTrue(_urlValidated);
            Assert.IsFalse(_viewModel.CanStart);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.InvalidUrl));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Constructor_InitialStateCheck()
        {
            //Arrange
            //Act
            //Assert
            Assert.IsTrue(_timerServiceMock.AddEventHandlerCalls == 1);
            Assert.IsTrue(_viewModelSubscribes);
            Assert.AreEqual(10d, _viewModel.Time);
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_viewModel.Url == null);
        }

        [Test]
        public void Start_UrlIsValidAndReturnsOk_Starts()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsFalse(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsTrue(_viewModel.CanStop);
            Assert.IsFalse(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x => 
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsValidAndReturnsBadRequest_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = _badRequestUrl;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Error
                    && x.Value.Status == HttpStatusCode.BadRequest
                    && x.Value.Url == _badRequestUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsValidAndReturnsAmbiguous_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = _ambiguousUrl;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Warning
                    && x.Value.Status == HttpStatusCode.Ambiguous
                    && x.Value.Url == _ambiguousUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsEmpty_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = string.Empty;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.InvalidUrl));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsNull_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = null;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.InvalidUrl));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsWhitespace_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = " ";

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.InvalidUrl));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_TimerIsAlreadyRunning_PreventsRecall()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Start();

            //Assert
            Assert.IsFalse(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsTrue(_viewModel.CanStop);
            Assert.IsFalse(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsInvalid_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = _invalidUrl;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.InvalidUrl));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_UrlIsValidButTestPingDoesNotIndicateSuccess_DoesNotStart()
        {
            //Arrange
            _viewModel.Url = _errorUrl;

            //Act
            _viewModel.Start();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Exceptions.FirstOrDefault(x => x.Value.Message == _errorUrl));

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Exceptions.FirstOrDefault(x => x.Value.Message == _errorUrl));
        }

        [Test]
        public void Stop_TimerIsNotRunning_PreventsCall()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Stop();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 0);

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Stop_TimerIsAlreadyRunning_Stops()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Stop();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 3);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Start_TimerWasRunningThenStopped_Starts()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Stop();
            _viewModel.Start();

            //Assert
            Assert.IsFalse(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsTrue(_viewModel.CanStop);
            Assert.IsFalse(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 4);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count(x => x.Value == Command.Start) == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 3);
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count(x => x.Value == Command.Start) == 2);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 2);
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }) == 2);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Reset_TimerWasRunningThenStopped_Resets()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Stop();
            _viewModel.Reset();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 4);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 3);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Reset_TimerWasRunningThenStoppedThenResetted_Resets()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Stop();
            _viewModel.Reset();
            _viewModel.Reset();

            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 5);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count(x => x.Value == Command.Reset) == 2);

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 4);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count(x => x.Value == Command.Reset) == 2);

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Stop_TimerWasRunningThenStopped_PreventsRecall()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Stop();
            _viewModel.Stop();

            //Assert
            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 3);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Stop));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Reset_TimerIsRunning_PreventsCall()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Start();
            _viewModel.Reset();

            //Assert
            //Assert
            Assert.IsFalse(_viewModel.CanChangeAddress);
            Assert.IsFalse(_viewModel.CanStart);
            Assert.IsTrue(_viewModel.CanStop);
            Assert.IsFalse(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == _viewModel.MultipliedTime);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Start));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Responses.FirstOrDefault(x =>
            {
                return x.Value.DateTime == _now
                    && x.Value.State == State.Ok
                    && x.Value.Status == HttpStatusCode.OK
                    && x.Value.Url == _okUrl;
            }));

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        [Test]
        public void Reset_TimerIsNotRunning_Resets()
        {
            //Arrange
            _viewModel.Url = _okUrl;

            //Act
            _viewModel.Reset();

            //Assert
            //Assert
            Assert.IsTrue(_viewModel.CanChangeAddress);
            Assert.IsTrue(_viewModel.CanStart);
            Assert.IsFalse(_viewModel.CanStop);
            Assert.IsTrue(_viewModel.CanReset);
            Assert.IsTrue(_timerServiceMock.Interval == 0);

            //Exceptions sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 0);

            //Commands sent to IUCMessageBoxViewModel
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Commands.Count == 2);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.ValidUrl));
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Commands sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Commands.Count == 1);
            Assert.IsNotNull(_messages_IUCLogPanelViewModel_Commands.FirstOrDefault(x => x.Value == Command.Reset));

            //Responses sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Responses.Count == 0);

            //Exceptions sent to IUCLogPanelViewModel
            Assert.IsTrue(_messages_IUCLogPanelViewModel_Exceptions.Count == 0);
        }

        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            _now = new DateTime(2021, 10, 11);
            _urlValidated = false;
            _viewModelSubscribes = false;

            _messages_IUCMessageBoxViewModel_Exceptions = new List<Message<IUCMessageBoxViewModel, Exception>>();
            _messages_IUCMessageBoxViewModel_Commands = new List<Message<IUCMessageBoxViewModel, Command>>();
            _messages_IUCLogPanelViewModel_Responses = new List<Message<IUCLogPanelViewModel, Response>>();
            _messages_IUCLogPanelViewModel_Commands = new List<Message<IUCLogPanelViewModel, Command>>();
            _messages_IUCLogPanelViewModel_Exceptions = new List<Message<IUCLogPanelViewModel, Exception>>();

            var okResponse = new Response
            {
                DateTime = _now,
                State = State.Ok,
                Status = HttpStatusCode.OK,
                Url = _okUrl
            };
            var okResult = new Result<Response>(okResponse);

            var badRequestResponse = new Response
            {
                DateTime = _now,
                State = State.Error,
                Status = HttpStatusCode.BadRequest,
                Url = _badRequestUrl
            };
            var badRequestResult = new Result<Response>(badRequestResponse);

            var ambiguousResponse = new Response
            {
                DateTime = _now,
                State = State.Warning,
                Status = HttpStatusCode.Ambiguous,
                Url = _ambiguousUrl
            };
            var ambiguousResult = new Result<Response>(ambiguousResponse);

            var timeoutResponse = new Response
            {
                DateTime = _now,
                State = State.Warning,
                Status = HttpStatusCode.Ambiguous,
                Url = _ambiguousUrl
            };
            var connectionErrorResult = new Result<Response>(null, new Exception(_errorUrl));

            var eventAggregatorMock = new Mock<IEventAggregatorAdapter>();
            eventAggregatorMock.Setup(x => x.PublishOnUIThreadAsync(It.IsAny<Message<IUCMessageBoxViewModel, Exception>>()))
                .Callback<object>(message =>
                {
                    _messages_IUCMessageBoxViewModel_Exceptions.Add((Message<IUCMessageBoxViewModel, Exception>)message);
                });
            eventAggregatorMock.Setup(x => x.PublishOnUIThreadAsync(It.IsAny<Message<IUCMessageBoxViewModel, Command>>()))
                .Callback<object>(message =>
                {
                    _messages_IUCMessageBoxViewModel_Commands.Add((Message<IUCMessageBoxViewModel, Command>)message);
                });
            eventAggregatorMock.Setup(x => x.PublishOnUIThreadAsync(It.IsAny<Message<IUCLogPanelViewModel, Response>>()))
                .Callback<object>(message =>
                {
                    _messages_IUCLogPanelViewModel_Responses.Add((Message<IUCLogPanelViewModel, Response>)message);
                });
            eventAggregatorMock.Setup(x => x.PublishOnUIThreadAsync(It.IsAny<Message<IUCLogPanelViewModel, Command>>()))
                .Callback<object>(message =>
                {
                    _messages_IUCLogPanelViewModel_Commands.Add((Message<IUCLogPanelViewModel, Command>)message);
                });
            eventAggregatorMock.Setup(x => x.PublishOnUIThreadAsync(It.IsAny<Message<IUCLogPanelViewModel, Exception>>()))
                .Callback<object>(message =>
                {
                    _messages_IUCLogPanelViewModel_Exceptions.Add((Message<IUCLogPanelViewModel, Exception>)message);
                });
            eventAggregatorMock.Setup(x => x.SubscribeOnPublishedThread(It.IsAny<object>()))
                .Callback<object>(obj =>
                {
                    _viewModelSubscribes = obj.GetType() == typeof(UCControlPanelViewModel);
                });

            var webSerivceMock = new Mock<IWebService>();
            webSerivceMock.Setup(x => x.CheckPageAsync(_okUrl)).Returns(Task.FromResult(okResult));
            webSerivceMock.Setup(x => x.CheckPageAsync(_badRequestUrl)).Returns(Task.FromResult(badRequestResult));
            webSerivceMock.Setup(x => x.CheckPageAsync(_ambiguousUrl)).Returns(Task.FromResult(ambiguousResult));
            webSerivceMock.Setup(x => x.CheckPageAsync(_errorUrl)).Returns(Task.FromResult(connectionErrorResult));

            var urlValidatorMock = new Mock<IUrlValidator>();
            urlValidatorMock.Setup(x => x.IsUrlValid(_okUrl)).Callback(() => _urlValidated = true).Returns(true);
            urlValidatorMock.Setup(x => x.IsUrlValid(_badRequestUrl)).Callback(() => _urlValidated = true).Returns(true);
            urlValidatorMock.Setup(x => x.IsUrlValid(_ambiguousUrl)).Callback(() => _urlValidated = true).Returns(true);
            urlValidatorMock.Setup(x => x.IsUrlValid(_invalidUrl)).Callback(() => _urlValidated = true).Returns(false);
            urlValidatorMock.Setup(x => x.IsUrlValid(_errorUrl)).Callback(() => _urlValidated = true).Returns(true);

            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock.Setup(x => x.GetNow()).Returns(_now);

            _timerServiceMock = new TimerServiceMock();

            _viewModel = new UCControlPanelViewModel(eventAggregatorMock.Object, webSerivceMock.Object, urlValidatorMock.Object, _timerServiceMock, dateTimeFactoryMock.Object);
        }
        #endregion
    }
}
