﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;
using WebWatcher.UI.ViewModels;

namespace WebWatcher.UI.Tests.ViewModels
{
    [TestFixture]
    public class UCLogPanelViewModelTests
    {
        private UCLogPanelViewModel _viewModel;

        private List<Message<IUCMessageBoxViewModel, Exception>> _messages_IUCMessageBoxViewModel_Exceptions;
        private bool _viewModelSubscribes;
        private DateTime _now;

        private Command _notDefinedCommand;

        private string _okUrl = "will return OK 200";
        private string _badRequestUrl = "will return Bad Request 400";
        private string _ambiguousUrl = "will return Ambiguous 300";

        private Response _okResponse;
        private Response _badRequestResponse;
        private Response _ambiguousResponse;
        private Response _urlNullResponse;

        private Exception _exception;

        #region Tests
        [Test]
        public async Task HandleAsyncResponse_ResponseIsOk_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Response>(_okResponse);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(_okUrl);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsAmbiguous_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Response>(_ambiguousResponse);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(_ambiguousUrl);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsBadRequest_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Response>(_badRequestResponse);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(_badRequestUrl);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseIsNull_PublishesArgumentNullException()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Response>(null);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Exceptions.FirstOrDefault(x =>
            {
                return x.Value.GetType() == typeof(ArgumentNullException)
                    && x.Value.Message == "Value cannot be null.";
            }));
        }

        [Test]
        public async Task HandleAsyncResponse_ResponseUrlIsNull_PublishesArgumentNullException()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Response>(_urlNullResponse);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Exceptions.FirstOrDefault(x =>
            {
                return x.Value.GetType() == typeof(ArgumentNullException)
                    && x.Value.Message == $"Value cannot be null.{Environment.NewLine}Parameter name: {nameof(Response.Url)}";
            }));
        }

        [Test]
        public void HandleAsyncResponse_MessageIsNull_ThrowsNullReferenceException()
        {
            //Arrage
            Message<IUCLogPanelViewModel, Response> message = null;
            var cancellationToken = default(CancellationToken);

            //Act
            //Assert
            Assert.ThrowsAsync<NullReferenceException>(async () => await _viewModel.HandleAsync(message, cancellationToken));
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsStart_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.Start);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(Command.Start.ToString());

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsStop_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.Stop);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(Command.Stop.ToString());

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsReset_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.Reset);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(Command.Reset.ToString());

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsInvalidUrl_DoesNotAppendAnything()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.InvalidUrl);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsValidUrl_DoesNotAppendAnything()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.ValidUrl);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsUrlFound_DoesNotAppendAnything()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.UrlFound);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsUrlNotFound_DoesNotAppendAnything()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(Command.UrlNotFound);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncCommand_CommandIsNotDefined_DoesNotAppendAnything()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Command>(_notDefinedCommand);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncException_ExceptionIsValid_AppendsNewMessageToLog()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Exception>(_exception);
            var cancellationToken = default(CancellationToken);
            var expectedContent = CustomMessage(_exception.Message);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == expectedContent);
            CollectionAssert.IsEmpty(_messages_IUCMessageBoxViewModel_Exceptions);
        }

        [Test]
        public async Task HandleAsyncException_ExceptionIsNull_PublishesArgumentNullException()
        {
            //Arrage
            var message = new Message<IUCLogPanelViewModel, Exception>(null);
            var cancellationToken = default(CancellationToken);

            //Act
            await _viewModel.HandleAsync(message, cancellationToken);

            //Assert
            Assert.IsTrue(_viewModel.Notes == string.Empty);
            Assert.IsTrue(_messages_IUCMessageBoxViewModel_Exceptions.Count == 1);
            Assert.IsNotNull(_messages_IUCMessageBoxViewModel_Exceptions.FirstOrDefault(x =>
            {
                return x.Value.GetType() == typeof(ArgumentNullException)
                    && x.Value.Message == $"Value cannot be null.";
            }));
        }

        [Test]
        public void HandleAsyncException_MessageIsNull_ThrowsNullReferenceException()
        {
            //Arrage
            Message<IUCLogPanelViewModel, Exception> message = null;
            var cancellationToken = default(CancellationToken);

            //Act
            //Assert
            Assert.ThrowsAsync<NullReferenceException>(async () => await _viewModel.HandleAsync(message, cancellationToken));
        }

        [Test]
        public void Constructor_InitialStateCheck()
        {
            //Arrange
            //Act
            //Assert
            Assert.IsTrue(_viewModelSubscribes);
            Assert.IsTrue(_viewModel.Notes == string.Empty);
        }
        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            _viewModelSubscribes = false;

            _now = new DateTime(2021, 7, 2);
            _notDefinedCommand = (Command)90;

            _messages_IUCMessageBoxViewModel_Exceptions = new List<Message<IUCMessageBoxViewModel, Exception>>();

            var eventAggregatorMock = new Mock<IEventAggregatorAdapter>();
            eventAggregatorMock.Setup(x => x.PublishOnUIThreadAsync(It.IsAny<Message<IUCMessageBoxViewModel, Exception>>()))
                .Callback<object>(message =>
                {
                    _messages_IUCMessageBoxViewModel_Exceptions.Add((Message<IUCMessageBoxViewModel, Exception>)message);
                });
            eventAggregatorMock.Setup(x => x.SubscribeOnPublishedThread(It.IsAny<object>()))
                .Callback<object>(obj =>
                {
                    _viewModelSubscribes = obj.GetType() == typeof(UCLogPanelViewModel);
                });

            var commandToTextResult_Empty = new Result<string>(string.Empty);
            var commandToTextResult_OnStart = new Result<string>(CustomMessage(Command.Start.ToString()));
            var commandToTextResult_OnStop = new Result<string>(CustomMessage(Command.Stop.ToString()));
            var commandToTextResult_OnReset = new Result<string>(CustomMessage(Command.Reset.ToString()));

            _okResponse = new Response
            {
                DateTime = _now,
                State = State.Ok,
                Status = HttpStatusCode.OK,
                Url = _okUrl
            };
            _badRequestResponse = new Response
            {
                DateTime = _now,
                State = State.Error,
                Status = HttpStatusCode.BadRequest,
                Url = _badRequestUrl
            };
            _ambiguousResponse = new Response
            {
                DateTime = _now,
                State = State.Warning,
                Status = HttpStatusCode.Ambiguous,
                Url = _ambiguousUrl
            };
            _urlNullResponse = new Response
            {
                DateTime = _now,
                State = State.Warning,
                Status = HttpStatusCode.Ambiguous,
                Url = null
            };

            var responseToText_ResponseNull = new Result<string>(null, new ArgumentNullException());
            var responseToText_UrlNull = new Result<string>(null, new ArgumentNullException(nameof(Response.Url)));
            var responseToText_StateOk = new Result<string>(CustomMessage(_okUrl));
            var responseToText_StateError = new Result<string>(CustomMessage(_badRequestUrl));
            var responseToText_StateWarning = new Result<string>(CustomMessage(_ambiguousUrl));

            _exception = new Exception("Valid exception");
            var exceptionToText_ExceptionValid = new Result<string>(CustomMessage(_exception.Message));
            var exceptionToText_ExceptionNull = new Result<string>(null, new ArgumentNullException());

            var logServiceMock = new Mock<IMessageService>();
            logServiceMock.Setup(x => x.CommandToText(It.IsAny<Command>()))
                .Returns<Command>(command => 
                {
                    switch (command)
                    {
                        case Command.Start:
                            return commandToTextResult_OnStart;
                        case Command.Stop:
                            return commandToTextResult_OnStop;
                        case Command.Reset:
                            return commandToTextResult_OnReset;
                        default:
                            return commandToTextResult_Empty;
                    }
                });
            logServiceMock.Setup(x => x.ResponseToText(null)).Returns(responseToText_ResponseNull);
            logServiceMock.Setup(x => x.ResponseToText(_okResponse)).Returns(responseToText_StateOk);
            logServiceMock.Setup(x => x.ResponseToText(_badRequestResponse)).Returns(responseToText_StateError);
            logServiceMock.Setup(x => x.ResponseToText(_ambiguousResponse)).Returns(responseToText_StateWarning);
            logServiceMock.Setup(x => x.ResponseToText(_urlNullResponse)).Returns(responseToText_UrlNull);
            logServiceMock.Setup(x => x.ExceptionToText(_exception)).Returns(exceptionToText_ExceptionValid);
            logServiceMock.Setup(x => x.ExceptionToText(null)).Returns(exceptionToText_ExceptionNull);

            _viewModel = new UCLogPanelViewModel(eventAggregatorMock.Object, logServiceMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _viewModel = null;
            _messages_IUCMessageBoxViewModel_Exceptions = null;
            _okResponse = null;
            _badRequestResponse = null;
            _ambiguousResponse = null;
            _exception = null;
        }

        private string CustomMessage(string input)
        {
            return $"This is note added upon receiving: {input}";
        }
        #endregion
    }
}
