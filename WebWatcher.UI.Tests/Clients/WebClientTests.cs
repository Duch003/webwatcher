﻿using Moq;
using NUnit.Framework;
using System;
using System.Net;
using System.Threading.Tasks;
using WebWatcher.UI.Clients;
using WebWatcher.UI.Interfaces;
using WebWatcher.UI.Models;
using WebWatcher.UI.Tests.Mocks;

namespace WebWatcher.UI.Tests.Clients
{
    public class WebClientTests
    {
        private string _success = "https://success/";
        private string _warning = "https://warning/";
        private string _error = "https://error/";
        private string _webExceptionNoResponse = "https://web_exception_no_response/";
        private string _webException = "https://web_exception/";
        private string _anyException = "https://exception/";
        private string _whitespace = " ";
        private string _invalidFormat = "abcd://";

        private IHttpWebResponseAdapter _successResponse;
        private IHttpWebResponseAdapter _warningResponse;
        private IHttpWebResponseAdapter _errorResponse;

        private DateTime _now;

        private CustomWebClient _client;

        #region Tests
        [Test]
        public async Task GetAsync_UrlIsValidAndReturnsOk_ReturnsHttpWebResponseAdapter()
        {
            //Arrage

            //Act
            var result = await _client.GetAsync(_success);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Ok);
            Assert.AreEqual(result.Status, HttpStatusCode.OK);
            Assert.AreEqual(result.Url, _success);
        }

        [Test]
        public async Task GetAsync_UrlIsValidAndReturnsAmbiguous_ReturnsHttpWebResponseAdapter()
        {
            //Arrage

            //Act
            var result = await _client.GetAsync(_warning);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Warning);
            Assert.AreEqual(result.Status, HttpStatusCode.Ambiguous);
            Assert.AreEqual(result.Url, _warning);
        }

        [Test]
        public async Task GetAsync_UrlIsValidAndReturnsNotFound_ReturnsHttpWebResponseAdapter()
        {
            //Arrage

            //Act
            var result = await _client.GetAsync(_error);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Error);
            Assert.AreEqual(result.Status, HttpStatusCode.NotFound);
            Assert.AreEqual(result.Url, _error);
        }

        [Test]
        public void GetAsync_UrlIsValidButConnectionErrorOnLocalComputerOccurs_ThrowsWebException()
        {
            //Arrage
            //Act
            //Assert
            var exception = Assert.ThrowsAsync<WebException>(async () => await _client.GetAsync(_webExceptionNoResponse));
            Assert.AreEqual(exception.Message, _webExceptionNoResponse);
        }

        [Test]
        public async Task GetAsync_UrlIsValidButConnectionErrorWithRemoteOccurs_ReturnsHttpWebResponseAdapter()
        {
            //Arrage
            //Act
            var result = await _client.GetAsync(_webException);

            //Assert
            Assert.AreEqual(result.DateTime, _now);
            Assert.AreEqual(result.State, State.Error);
            Assert.AreEqual(result.Status, HttpStatusCode.NotFound);
            Assert.AreEqual(result.Url, _webException);
        }

        [Test]
        public void GetAsync_UrlIsValidButErrorOnLocalComputerOccurs_ThrowsException()
        {
            //Arrage
            //Act
            //Assert
            var exception = Assert.ThrowsAsync<Exception>(async () => await _client.GetAsync(_anyException));
            Assert.AreEqual(exception.Message, _anyException);
        }

        [Test]
        public void GetAsync_UrlIsWhitespace_ThrowsUriFormatException()
        {
            //Arrage
            //Act
            //Assert
            var exception = Assert.ThrowsAsync<UriFormatException>(async () => await _client.GetAsync(_whitespace));
            Assert.AreEqual(exception.Message, "Invalid URI: The format of the URI could not be determined.");
        }

        [Test]
        public void GetAsync_UrlIsEmpty_ThrowsUriFormatException()
        {
            //Arrage
            //Act
            //Assert
            var exception = Assert.ThrowsAsync<UriFormatException>(async () => await _client.GetAsync(string.Empty));
            Assert.AreEqual(exception.Message, "Invalid URI: The URI is empty.");
        }

        [Test]
        public void GetAsync_UrlIsNull_ThrowsArgumentNullException()
        {
            //Arrage
            //Act
            //Assert
            var exception = Assert.ThrowsAsync<ArgumentNullException>(async () => await _client.GetAsync(null));
            Assert.AreEqual(exception.Message, "Value cannot be null.");
        }

        [Test]
        public void GetAsync_UrlHasInvalidFormat_ThrowsNotSupportedException()
        {
            //Arrage
            //Act
            //Assert
            var exception = Assert.ThrowsAsync<NotSupportedException>(async () => await _client.GetAsync(_invalidFormat));
            Assert.AreEqual(exception.Message, "The URI prefix is not recognized.");
        }
        #endregion

        #region Private methods
        [SetUp]
        public void Setup()
        {
            _now = new DateTime(2021, 10, 11);

            _successResponse = new HttpWebResponseMock(HttpStatusCode.OK, new Uri(_success));
            _warningResponse = new HttpWebResponseMock(HttpStatusCode.Ambiguous, new Uri(_warning));
            _errorResponse = new HttpWebResponseMock(HttpStatusCode.NotFound, new Uri(_error));

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.Setup(x => x.StatusCode).Returns(HttpStatusCode.NotFound);
            mockHttpWebResponse.Setup(x => x.ResponseUri).Returns(new Uri(_webException));

            var webRequestorMock = new Mock<IWebRequestor>();
            webRequestorMock.Setup(x => x.GetAsync(_success)).Returns(Task.FromResult(_successResponse));
            webRequestorMock.Setup(x => x.GetAsync(_warning)).Returns(Task.FromResult(_warningResponse));
            webRequestorMock.Setup(x => x.GetAsync(_error)).Returns(Task.FromResult(_errorResponse));
            webRequestorMock.Setup(x => x.GetAsync(_anyException)).Throws(new Exception(_anyException));
            webRequestorMock.Setup(x => x.GetAsync(_webException))
                .Throws(new WebException(_webException, null, WebExceptionStatus.Timeout, mockHttpWebResponse.Object));
            webRequestorMock.Setup(x => x.GetAsync(_webExceptionNoResponse)).Throws(new WebException(_webExceptionNoResponse));
            webRequestorMock.Setup(x => x.GetAsync(string.Empty)).Throws(new UriFormatException("Invalid URI: The URI is empty."));
            webRequestorMock.Setup(x => x.GetAsync(_whitespace)).Throws(new UriFormatException("Invalid URI: The format of the URI could not be determined."));
            webRequestorMock.Setup(x => x.GetAsync(_invalidFormat)).Throws(new NotSupportedException("The URI prefix is not recognized."));
            webRequestorMock.Setup(x => x.GetAsync(null)).Throws(new ArgumentNullException());

            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock.Setup(x => x.GetNow()).Returns(_now);

            _client = new CustomWebClient(webRequestorMock.Object, dateTimeFactoryMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _successResponse = null;
            _warningResponse = null;
            _errorResponse = null;
            _client = null;
        }
        #endregion
    }
}
